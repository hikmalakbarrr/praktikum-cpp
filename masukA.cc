/*
   Nama Program : masukA.cc
   Tgl buat     : 10 Oktober 2023
   Deskripsi    : memeriksa inputan
*/

#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
  system("clear");

  char A = ' ';

  cout << "Masukkan Suatu Karakter : "; cin >> A;
  if (A == 'A')
  {
    cout << "Anda menekan A besar" << endl;
  } else {
    cout << "Anda tidak menekan A besar" << endl;
  }

  return 0;
}