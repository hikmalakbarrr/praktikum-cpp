#include <stdio.h>

unsigned long long kombinasi(int n, int r) {
    if (r == 0 || r == n) {
        return 1;
    } else {
        return kombinasi(n - 1, r - 1) + kombinasi(n - 1, r);
    }
}

int main() {
    int tingkat;

    printf("Masukkan tingkat segitiga Pascal: ");
    scanf("%d", &tingkat);

    for (int i = 0; i < tingkat; i++) {
        
        for (int j = 0; j < tingkat - i - 1; j++) {
            printf(" ");
        }

        for (int j = 0; j <= i; j++) {
            printf("%llu ", kombinasi(i, j));
        }

        printf("\n");
    }

    return 0;
}
